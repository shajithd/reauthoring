<?php include 'words_database.php'; ?>

<?php

//Creating variables

$activityid = $_POST['activityid'];

echo $activityid;

print_r($_POST);

//Checking input field
function checkResponse($response){

	if(!isset($response) || trim($response) == ''){
		echo "<a href = ../Readings/index.html> Go back!</a>";
		exit("You did not fill out the required field");
	}	
}

//Connecting to DB
function connectDB(){

	$connect=mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_DATABASE);

	if(mysqli_connect_errno($connect)){

		exit("Failed to connect");
	}

	else{

		return $connect;
	}
}

checkResponse($activityid);

$connect= connectDB();

$retrieve = "SELECT response, COUNT(*) AS freq FROM `" .$activityid . "` GROUP BY response";

$result = mysqli_query($connect,$retrieve);

if(empty($result)){
	echo "<p>" . $activityid . "table does not exist</p>";

}else {
	echo "<table border='1'>
	<tr>
	<th>Response</th>
	<th>Frequency</th>
	</tr>";

	while($row = mysqli_fetch_array($result)){

		echo "<tr>";
		echo "<td>" . $row['response'] . "</td>";
		echo "<td>" . $row['freq'] . "</td>";
		echo "</tr>";
	}
}