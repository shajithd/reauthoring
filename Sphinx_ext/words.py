# Copyright (C) 2014 The University of Sydney
# This file is part of the Reauthoring toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Shajith Dissanayake (sdis7497@uni.sydney.edu.au)

from __future__ import division

import os, sys, posixpath
from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst import roles
from sphinx.util.compat import Directive
from docutils.utils import relative_path

from Sphinx_ext import html_form, common


class words(nodes.General, nodes.Element):
    pass

def visit_words_node(self, node):
    """
    Function executed when the node representing the :words:`title` is visited
    """

    reading_id = node["args"][0]

    if reading_id == 'get':
                    # Deploy the div with the script inside
        self.body.append('<form action="../_static/words_retrieve.php" method="post" id="form_get">')
        self.body.append('<div>')
        self.body.append('<label for="words_get">Activity ID:</label>')
        self.body.append('<input name="activityid" id="words_get">')
        self.body.append('</div>')
        self.body.append('<br></br>')
        self.body.append('<div>')
        self.body.append('<input type="submit" value="Submit">')
        self.body.append('</div>')
        self.body.append('</form>')
        self.body.append('<br></br>')

    elif "|" in reading_id:

        params=reading_id.split("|")
        activity_id = params[0]
        correct_responses = params[1].split(",")

        self.body.append('<form action="../_static/words_check.php" method="post" id="form_check">')
        self.body.append('<div>')
        self.body.append('<label for="words_check">Answer:</label>')
        self.body.append('<input name="response" id="words_check_input">')
        self.body.append('</div>')
        self.body.append('<div style="display:none">')

        for i in correct_responses:
            self.body.append('<input name="correct[]" value=%s>' %i)


        self.body.append('</div>')
        self.body.append('<div style="display:none">')
        self.body.append('<input name="activityid" id="words_check_activityid" value=%s>' %activity_id)
        self.body.append('</div>')
        self.body.append('<br></br>')
        self.body.append('<div>')
        self.body.append('<input type="submit" value="Submit">')
        self.body.append('</div>')
        self.body.append('</form>')
        self.body.append('<br></br>')


    else:


        # Deploy the div with the script inside
        self.body.append('<form action="../_static/words_process.php" method="post" id="form_id">')
        self.body.append('<div>')
        self.body.append('<label for="words_response">Answer:</label>')
        self.body.append('<input name="response" id="words_response">')
        self.body.append('</div>')
        self.body.append('<div style="display:none">')
        self.body.append('<input name="activityid" id="words_response" value=%s>' %reading_id)
        self.body.append('</div>')
        self.body.append('<br></br>')
        self.body.append('<div>')
        self.body.append('<input type="submit" value="Submit">')
        self.body.append('</div>')
        self.body.append('</form>')
        self.body.append('<br></br>')

        return



def depart_words_node(self,node):
    """
    Function executed when the node representing .. words:: 'title' is left
    """
    pass

class wordsDirective(Directive):
    """
    Directive to insert a form which captures keywords
    entered by students.
    The syntax is:

    .. words:: reading-id
    """

    has_content = False
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False

    def run(self):

        body = words(args = self.arguments, name = self.name)


        return [body]



def setup(app):

    app.add_node(words,
                 html = (visit_words_node,
                         depart_words_node))

    app.add_directive('words', wordsDirective)

    return {'version' : '0.1'} #identifies the version of this extension