
***************************
Analytics in Comprehension
***************************

The following text is an excerpt from 'Essentials of Computer Organization and Architecture' by Linda Null, pages 12-14::



   Generation Zero: Mechanical Calculating Machines (1642–1945)

   Prior to the 1500s, a typical European businessperson used an abacus for calcu-
   lations and recorded the result of his ciphering in Roman numerals. After the
   decimal numbering system finally replaced Roman numerals, a number of peo-
   ple invented devices to make decimal calculations even faster and more accu-
   rate. Wilhelm Schickard (1592–1635) has been credited with the invention of the
   first mechanical calculator, the Calculating Clock (exact date unknown). This
   device was able to add and subtract numbers containing as many as six digits. In
   1642, Blaise Pascal (1623–1662) developed a mechanical calculator called the
   Pascaline to help his father with his tax work. The Pascaline could do addition
   with carry and subtraction. It was probably the first mechanical adding device
   actually used for a practical purpose. In fact, the Pascaline was so well con-
   ceived that its basic design was still being used at the beginning of the twentieth
   century, as evidenced by the Lightning Portable Adder in 1908, and the Addome-
   ter in 1920. Gottfried Wilhelm von Leibniz (1646–1716), a noted mathemati-
   cian, invented a calculator known as the Stepped Reckoner that could add,
   subtract, multiply, and divide. None of these devices could be programmed or
   had memory. They required manual intervention throughout each step of their
   calculations.

   Although machines like the Pascaline were used into the twentieth century,
   new calculator designs began to emerge in the nineteenth century. One of the
   most ambitious of these new designs was the Difference Engine by Charles Bab-
   bage (1791–1871). Some people refer to Babbage as “the father of computing.”
   By all accounts, he was an eccentric genius who brought us, among other things,
   the skeleton key and the “cow catcher,” a device intended to push cows and other
   movable obstructions out of the way of locomotives.
   Babbage built his Difference Engine in 1822. The Difference Engine got its
   name because it used a calculating technique called the method of differences. 
   The machine was designed to mechanize the solution of polynomial functions and was
   actually a calculator, not a computer. Babbage also designed a general-purpose
   machine in 1833 called the Analytical Engine. Although Babbage died before he
   could build it, the Analytical Engine was designed to be more versatile than his
   earlier Difference Engine. The Analytical Engine would have been capable of per-
   forming any mathematical operation. The Analytical Engine included many of the
   components associated with modern computers: an arithmetic processing unit to
   perform calculations (Babbage referred to this as the mill), a memory (the store),
   and input and output devices. Babbage also included a conditional branching
   operation where the next instruction to be performed was determined by the result
   of the previous operation. Ada, Countess of Lovelace and daughter of poet Lord
   Byron, suggested that Babbage write a plan for how the machine would calculate
   numbers. This is regarded as the first computer program, and Ada is considered to
   be the first computer programmer. It is also rumored that she suggested the use of
   the binary number system rather than the decimal number system to store data.
   A perennial problem facing machine designers has been how to get data into
   the machine. Babbage designed the Analytical Engine to use a type of punched
   card for input and programming. Using cards to control the behavior of a machine
   did not originate with Babbage, but with one of his friends, Joseph-Marie
   Jacquard (1752–1834). In 1801, Jacquard invented a programmable weaving
   loom that could produce intricate patterns in cloth. Jacquard gave Babbage a tap-
   estry that had been woven on this loom using more than 10,000 punched cards.
   To Babbage, it seemed only natural that if a loom could be controlled by cards,
   then his Analytical Engine could be as well. Ada expressed her delight with this
   idea, writing, “[T]he Analytical Engine weaves algebraical patterns just as the
   Jacquard loom weaves flowers and leaves.”
   The punched card proved to be the most enduring means of providing input to a
   computer system. Keyed data input had to wait until fundamental changes were
   made in how calculating machines were constructed. In the latter half of the nine-
   teenth century, most machines used wheeled mechanisms, which were difficult to
   integrate with early keyboards because they were levered devices. But levered
   devices could easily punch cards and wheeled devices could easily read them. So a
   number of devices were invented to encode and then “tabulate” card-punched data.
   The most important of the late-nineteenth-century tabulating machines was the one
   invented by Herman Hollerith (1860–1929). Hollerith’s machine was used for
   encoding and compiling 1890 census data. This census was completed in record
   time, thus boosting Hollerith’s finances and the reputation of his invention. Hollerith
   later founded the company that would become IBM. His 80-column punched card,
   the Hollerith card, was a staple of automated data processing for over 50 years.

Collecting Student Responses
============================ 
The directive to insert a form to collect student responses is::

  .. words:: analytics-in-comprehension


Make sure the string that follows the `.. words::` directive is unique throughout
your documents. After processing, the result is:

.. words:: analytics-in-comprehension

Retrieving Student Responses
============================

The directive to insert a form to retrieve student responses is::

  .. words:: get

In this case the string that follows the `.. words::` directive HAS to be `get`. The 'Activity ID' in the form refers to the string entered after `..words::`. Looking at the previous example, this would be `analytics-in-comprehension`. When submitted, all student responses will be displayed along with the frequency of the submitted keywords.

After processing, the result is:

.. words:: get

Keyword Question with Multiple Correct Answers
==============================================

The directive to insert a keywords choice question with single/multiple correct answers
is::

  .. words:: exampleid|correct1,correct2,correct3

The `exampleid` refers to the Activity ID given to this particular task. Make sure that '|' follows the Activity ID if you want to specify correct responses. If specifying multiple correct answers, just separate each with a comma as in the above example.

After processing, the result is:

.. words:: exampleid|correct1,correct2,correct3

